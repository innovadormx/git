# Generated by Django 3.1.2 on 2021-04-07 19:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('control', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='prospecto',
            name='fecha',
            field=models.CharField(default='07 de abril 2021 - 14:19', max_length=60),
        ),
    ]
