# Generated by Django 3.1.2 on 2021-04-07 19:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('control', '0002_prospecto_fecha'),
    ]

    operations = [
        migrations.AlterField(
            model_name='prospecto',
            name='fecha',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
