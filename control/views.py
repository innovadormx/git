from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import *
from .forms import *
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from datetime import datetime, timedelta
import json
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/login')
def inicio(request):
    prospectos = Prospecto.objects.all()
    context = {'prospectos': prospectos}
    return render(request, 'control/inicio.html', context)

    
@csrf_exempt
@require_POST
def new_lead(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    prospecto = Prospecto()
    prospecto.nombre = body['entry'].get("name")
    prospecto.phone = body['entry'].get("phone")
    prospecto.save()
    return HttpResponse(status=200)


@login_required(login_url='/login')
def editar_prospecto(request, pk):
    prospecto = Prospecto.objects.get(id=pk)
    
    form = FormProspecto(instance=prospecto)
    
    if request.method == 'POST':
        form = FormProspecto(request.POST, instance=prospecto)
        if form.is_valid():
            form.save()
            return redirect('/')
        
    context = {'form':form}
    return render(request, 'control/editar_prospecto.html', context)


@login_required(login_url='/login')
def agregar_venta(request, pk):
    prospecto = Prospecto.objects.get(id=pk)
    
    entrega = str(datetime.now() + timedelta(days=1))[:-16]
    
    data = {'nombre': prospecto.nombre,
            'telefono': prospecto.phone,
            'tipo_servicio': 'Dia Siguiente',
            'fecha_entrega': entrega}
    
    form = FormVenta(data, initial=data)
    
    
    if request.method == 'POST':
        form = FormVenta(request.POST)
        if form.is_valid():
            prospecto.concretada = True
            prospecto.save()
            form.save()
            return redirect('/ventas')
    
    context = {'form':form}
    return render(request, 'control/agregar_venta.html', context)


@login_required(login_url='/login')
def ventas(request):
    ventas = VentaRealizada.objects.all()
    context = {'ventas': ventas}
    return render(request, 'control/ventas.html', context)

''''
def agregar_venta(request, pk):
    prospecto = Prospecto.objects.get(id=pk)
    nombre_prospecto = prospecto.nombre
    telefono_prospecto = prospecto.phone
    
    ventas = VentaRealizada.objects.all()
    form = FormVenta()
    
    if request.method == 'POST':
        form = FormVenta(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
        
    context = {'form':form}
    return render(request, 'control/agregar_venta.html', context)

'''