from django.contrib import admin
from django.urls import path, include
from . import views
from django.contrib.auth.views import LoginView

urlpatterns = [
    path('', views.inicio),
    path('api/new_lead', views.new_lead, name='new_lead'),
    path('editar_prospecto/<str:pk>', views.editar_prospecto, name='editar_prospecto'),
    path('agregar_venta/<str:pk>', views.agregar_venta, name='agregar_entrega'),
    path('ventas', views.ventas),
    path('login/', LoginView.as_view(template_name="control/login.html"), name='login')
]