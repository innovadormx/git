from django import forms
from django.forms import ModelForm
from .models import *


class FormProspecto(forms.ModelForm):
    class Meta:
        model = Prospecto
        fields = '__all__'
        
        
class FormVenta(forms.ModelForm):
    class Meta:
        model = VentaRealizada
        fields = '__all__'