from django.db import models
from datetime import datetime
from django.utils import timezone
from datetime import datetime


class Prospecto(models.Model):
    nombre = models.CharField(max_length=60, editable=False)
    phone = models.CharField(max_length=12, editable=False)
    nota = models.CharField(default='No se ha hecho el intento de llamada', max_length=1000, blank=True, null=True)
    fecha = models.DateTimeField(auto_now_add=True, editable=False)
    concretada = models.BooleanField(default=False)
    updated_date = models.DateTimeField(auto_now=True)
  
    def __str__(self):
        return self.nombre
    
    
class VentaRealizada(models.Model):
    nombre = models.CharField(max_length=60, default='Ingrese Nombre')
    telefono = models.CharField(max_length=12, default='477000000')
    calle = models.CharField(max_length=100)
    numero = models.CharField(max_length=100)
    colonia = models.CharField(max_length=100)
    ciudad = models.CharField(max_length=100)
    cantidad = models.CharField(max_length=100)
    referencia = models.CharField(max_length=100)
    nota = models.CharField(max_length=100)
    fecha_creada = models.DateTimeField(auto_now_add=True, editable=False)
    fecha_entrega = models.CharField(max_length=100, default='Mañana')
    tipo_servicio = models.CharField(max_length=100, default='Dia Siguiente')
    
    
    def __str__(self):
        return self.nombre
